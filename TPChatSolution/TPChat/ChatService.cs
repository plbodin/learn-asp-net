﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPChat.Models;

namespace TPChat
{
    public class ChatService
    {
        #region Singleton Pattern
        // On mets le constructeur en privé
        private ChatService() { }
        // Static pour que l'Instance soit partagée pour tous
        // Privée, pour que ChatService gère son implémentation
        private static ChatService _Instance;

        // Propriété statique qui retourne le membre privé _Instance
        public static ChatService Instance
        {
            get
            {
                // Premier appel, _Instance est null
                if (_Instance == null)
                {
                    _Instance = new ChatService();
                }

                return _Instance;
            }
        }
        #endregion

        private List<Chat> _chats = Chat.GetMeuteDeChats();

        public List<Chat> GetChats()
        {
            return _chats;
        }

        public Chat GetChat(int? id)
        {
            return _chats.FirstOrDefault(c => c.Id == id);
        }

        public void Delete(int id)
        {
            // On récupère le premier chat (ou null) 
            // correspondant à l'id
            Chat chat = this._chats.FirstOrDefault(c => c.Id == id);

            // Une bonne pratique, est de tester la nullabilité d'un objet            
            if (chat != null)
            {
                this._chats.Remove(chat);
            }
        }
    }
}