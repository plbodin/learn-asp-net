﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPChat.Models;

namespace TPChat.Controllers
{
    public class ChatController : Controller
    {
        // GET: Chat
        public ActionResult Index()
        {
            List<Chat> model = ChatService.Instance.GetChats();
            return View(model);
        }

        // GET: Chat/Details/5
        public ActionResult Details(int? id)
        {
            // On récupère le chat
            Chat model = ChatService.Instance.GetChat(id);

            // Si le chat n'existe pas, on redirige 
            // vers la page "Liste des chats"
            if (model == null)
            {
                return this.RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            // On récupère le chat
            Chat model = ChatService.Instance.GetChat(id);

            if (model == null)
            {
                return this.RedirectToAction(nameof(Index));
            }

            return View(model);
        }

      
        // GET: Chat/Delete/5
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            // On récupère le chat
            Chat model = ChatService.Instance.GetChat(id);

            if (model == null)
            {
                return this.RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        // POST: Chat/Delete/5
        [HttpPost]
        // Permets de vérifier que le formulaire posté contient bien le token
        // de validation valide.
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // Appel (délégation) au service métier
                ChatService.Instance.Delete(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}
