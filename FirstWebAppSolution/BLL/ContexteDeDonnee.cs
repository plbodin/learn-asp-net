﻿using BLL.Models;
using System.Data.Entity;
using System.Diagnostics;

namespace BLL
{
    public class ContexteDeDonnee : DbContext
    {
        public DbSet<Animal> Animaux { get; set; }
        public DbSet<Couleur> Couleurs { get; set; }
        public DbSet<TypeAnimal> TypeAnimaux { get; set; }

        /// <summary>
        /// Le : base() permets d'appeler le constructeur de la classe héritée.
        /// Ici la classe héritée est DbContext, c'est donc le constructeur
        /// de la classe DbContext.
        /// </summary>
        public ContexteDeDonnee() : this("DefaultConnectionString")
        {           
        }

        public ContexteDeDonnee(string connectionStringName)
            : base(connectionStringName)
        {
            // On loggue dans la fenêtre de Debug les requêtes SQL exécutées
            this.Database.Log = s => Debug.WriteLine(s);

            // Désactive le chargement implicite des données
            this.Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        /// Override du OnModelCreating pour faire du FluentAPI
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // On spécifie que un animal a plusieurs couleurs
            // Et qu'une couleur est liée a plusieurs animaux
            modelBuilder.Entity<Animal>().HasMany(a => a.Couleurs).WithMany();
            
        }
    }
}
