﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BLL.Services
{
    public class AnimalService
    {
        private ContexteDeDonnee _Contexte = new ContexteDeDonnee();
        #region Singleton
        private AnimalService() { }
        private static AnimalService _Instance;
        public static AnimalService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AnimalService();
                }
                return _Instance;
            }
        }
        #endregion

        public List<Couleur> GetCouleurs()
        {
            return this._Contexte.Couleurs.ToList();
        }

        public List<TypeAnimal> GetTypeAnimaux()
        {
            return this._Contexte.TypeAnimaux.ToList(); ;
        }

        public List<Animal> GetAnimaux()
        {
            // On a une requête SQL qui part au serveur
           /* List<Animal> animaux = this._Contexte.Animaux.ToList();

            foreach(Animal animal in animaux)
            {
                // Le .Load() permets de charger dans le contexte les couleurs de l'animal "animal"
                this._Contexte.Entry(animal).Collection(a => a.Couleurs).Load();

                this._Contexte.Entry(animal).Reference(a => a.TypeAnimal).Load();
            }*/

            // EagerLoading. Include() se trouve dans le namespace System.Data.Entity
            return this._Contexte.Animaux.Include(a => a.TypeAnimal)
                                         .Include(a => a.Couleurs)   
                                         .OrderBy(c => c.Id)
                                         //.Skip(2)
                                         //.Take(30)
                                         .ToList();            
        }

        public Animal GetAnimal(int? id)
        {
            // Chargement de l'animal, avec inclusion du TypeAnimal et des Couleurs dans
            // la même requête SQL.
            Animal animal = this._Contexte.Animaux.Find(id);

            // Chargement explicite des couleurs de l'animal dans le contexte
            this._Contexte.Entry(animal).Collection(a => a.Couleurs).Load();

            // Chargement explicite du type animal
            this._Contexte.Entry(animal).Reference(a => a.TypeAnimal).Load();

            return animal;
        }

        public void CreateAnimal(Animal animal)
        {          
            // Ajout de l'animal dans le contexte
            this._Contexte.Animaux.Add(animal);
            this._Contexte.SaveChanges();
        }

        public void UpdateAnimal(Animal animal)
        {
            // L'animal reçu ici est censé être déjà dans le contexte
            // On fait seulement la sauvegarde en base
            this._Contexte.SaveChanges();
        }

        public void DeleteAnimal(int? id)
        {            
            // Charge l'animal dans le contexte
            Animal animal = this.GetAnimal(id);

            // On vérifie que l'animal existe bien avant de le supprimer
            if (animal != null)
            {
                // On dit au contexte que l'on supprime l'animal
                this._Contexte.Animaux.Remove(animal);
                this._Contexte.SaveChanges();
            }
        }
    }
}