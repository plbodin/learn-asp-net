﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BLL.Models
{
    [Table("Animaux")]
    public class Animal
    {
        public int Id { get; set; }
      
        public string Nom { get; set; }

        public string Description { get; set; }

        [NotMapped] // Attribut pour ignorer la propriété dans la BDD
        public string NomPrenom { get; set; }

        
        public virtual TypeAnimal TypeAnimal { get; set; }
       
        public virtual List<Couleur> Couleurs { get; set; }

        public override string ToString()
        {
            return $"Id={Id},Nom={Nom},Description={Description},TypeAnimal={TypeAnimal}";
        }

        [NotMapped]
        public ETypeAnimal ETypeAnimal { get; set; }
    }

    public enum ETypeAnimal
    {
        Chien,
        Chat,
        Souris
    }
}