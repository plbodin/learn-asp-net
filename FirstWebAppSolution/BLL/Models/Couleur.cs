﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BLL.Models
{
    public class Couleur
    {
        public int Id { get; set; }
        [Column("Nom_Couleur")]
        public string Nom { get; set; }

        // public List<Animal> Animaux { get; set; }
    }
}