﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BLL.Models
{
    [Table("TypeAnimaux")]
    public class TypeAnimal
    {
        public int Id { get; set; }
        public string Nom { get; set; }
    }
}