﻿namespace BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.CouleurAnimals", newName: "AnimalCouleurs");
            RenameColumn(table: "dbo.Couleurs", name: "Nom", newName: "Nom_Couleur");
            DropPrimaryKey("dbo.AnimalCouleurs");
            AddPrimaryKey("dbo.AnimalCouleurs", new[] { "Animal_Id", "Couleur_Id" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.AnimalCouleurs");
            AddPrimaryKey("dbo.AnimalCouleurs", new[] { "Couleur_Id", "Animal_Id" });
            RenameColumn(table: "dbo.Couleurs", name: "Nom_Couleur", newName: "Nom");
            RenameTable(name: "dbo.AnimalCouleurs", newName: "CouleurAnimals");
        }
    }
}
