﻿namespace BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animaux",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        Description = c.String(),
                        TypeAnimal_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TypeAnimaux", t => t.TypeAnimal_Id)
                .Index(t => t.TypeAnimal_Id);
            
            CreateTable(
                "dbo.Couleurs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TypeAnimaux",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CouleurAnimals",
                c => new
                    {
                        Couleur_Id = c.Int(nullable: false),
                        Animal_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Couleur_Id, t.Animal_Id })
                .ForeignKey("dbo.Couleurs", t => t.Couleur_Id, cascadeDelete: true)
                .ForeignKey("dbo.Animaux", t => t.Animal_Id, cascadeDelete: true)
                .Index(t => t.Couleur_Id)
                .Index(t => t.Animal_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Animaux", "TypeAnimal_Id", "dbo.TypeAnimaux");
            DropForeignKey("dbo.CouleurAnimals", "Animal_Id", "dbo.Animaux");
            DropForeignKey("dbo.CouleurAnimals", "Couleur_Id", "dbo.Couleurs");
            DropIndex("dbo.CouleurAnimals", new[] { "Animal_Id" });
            DropIndex("dbo.CouleurAnimals", new[] { "Couleur_Id" });
            DropIndex("dbo.Animaux", new[] { "TypeAnimal_Id" });
            DropTable("dbo.CouleurAnimals");
            DropTable("dbo.TypeAnimaux");
            DropTable("dbo.Couleurs");
            DropTable("dbo.Animaux");
        }
    }
}
