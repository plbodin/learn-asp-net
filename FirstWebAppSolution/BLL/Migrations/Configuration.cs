﻿namespace BLL.Migrations
{
    using BLL.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BLL.ContexteDeDonnee>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BLL.ContexteDeDonnee context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            // On ajoute/mets à jour en base des couleurs
            // en se basant sur le nom pour respecter le coté unique de l'objet
            context.Couleurs.AddOrUpdate(c => c.Nom, new Couleur[]
            {
                new Couleur { Nom = "Rose" },
                new Couleur { Nom = "Jaune" },
                new Couleur { Nom = "Vert" },
                new Couleur { Nom = "Bleu" }
            });

            context.TypeAnimaux.AddOrUpdate(t => t.Nom, new TypeAnimal[]
            {
                new TypeAnimal { Nom = "Chien" },
                new TypeAnimal { Nom = "Chat" },
                new TypeAnimal { Nom = "Oiseau" },
                new TypeAnimal { Nom = "Poisson" },
                new TypeAnimal { Nom = "Autre" }
            });

            context.SaveChanges();
        }
    }
}
