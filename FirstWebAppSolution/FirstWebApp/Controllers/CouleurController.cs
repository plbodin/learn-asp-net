﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;

namespace FirstWebApp.Controllers
{
    public class CouleurController : Controller
    {
        private ContexteDeDonnee db = new ContexteDeDonnee();

        // GET: Couleur
        public ActionResult Index()
        {
            return View(db.Couleurs.ToList());
        }

        // GET: Couleur/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Couleur couleur = db.Couleurs.Find(id);
            if (couleur == null)
            {
                return HttpNotFound();
            }
            return View(couleur);
        }

        // GET: Couleur/Create
        public ActionResult Create()
        {
            return View(new Couleur());
        }

        // POST: Couleur/Create
        // Afin de déjouer les attaques par survalidation, activez les propriétés spécifiques auxquelles vous voulez établir une liaison. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nom")] Couleur couleur)
        {
            if (ModelState.IsValid)
            {
                db.Couleurs.Add(couleur);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(couleur);
        }

        // GET: Couleur/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Couleur couleur = db.Couleurs.Find(id);
            if (couleur == null)
            {
                return HttpNotFound();
            }
            return View(couleur);
        }

        // POST: Couleur/Edit/5
        // Afin de déjouer les attaques par survalidation, activez les propriétés spécifiques auxquelles vous voulez établir une liaison. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nom")] Couleur couleur)
        {
            if (ModelState.IsValid)
            {
                db.Entry(couleur).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(couleur);
        }

        // GET: Couleur/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Couleur couleur = db.Couleurs.Find(id);
            if (couleur == null)
            {
                return HttpNotFound();
            }
            return View(couleur);
        }

        // POST: Couleur/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Couleur couleur = db.Couleurs.Find(id);
            db.Couleurs.Remove(couleur);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
