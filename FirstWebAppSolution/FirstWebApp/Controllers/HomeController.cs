﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstWebApp.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            if (!this.TempData.ContainsKey("cle1"))
            {
                this.TempData.Add("cle1", "valeur 1");
            }
        }

        public ActionResult Index()
        {
            if (ViewBag.cle1 == null)
            {
                ViewBag.cle1 = "valeur 1";
            }

            return View();
        }

        public ActionResult Coucou(string id, string name)
        {
            ViewBag.Message = name;

            return View("About");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Test()
        {
            return View("Contact");
            return new JsonResult()
            {
                Data = new
                {
                    Toto = "titi"
                }
            };
        }
    }
}