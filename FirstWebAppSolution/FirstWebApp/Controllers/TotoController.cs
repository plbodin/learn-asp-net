﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstWebApp.Controllers
{     
    public class TotoController : Controller
    {       
        public ActionResult Titi()
        {
            ViewBag.TestHtml = new HtmlString("<h1>Test Html</h1>");
            return View();
        }

        public ActionResult Test()
        {
            return View("~/Home/Contact");
        }

        public ActionResult Test2()
        {
            return View("~/Home/Contact");
        }
    }
}