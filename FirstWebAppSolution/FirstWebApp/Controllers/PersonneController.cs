﻿using FirstWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstWebApp.Controllers
{
    public class PersonneController
        : Controller
    {
        public ActionResult Index()
        {
            PersonneViewModel vm = new PersonneViewModel
            {
                Nom = "Personne",
                Animaux = new List<AnimalViewModel>
                {
                    new AnimalViewModel { Nom = "Animal 1" },
                    new AnimalViewModel { Nom = "Animal 2" }
                }
            };

            // On passe le model à la vue
            return View(vm);
        }
    }
}