﻿using BLL.Models;
using BLL.Services;
using FirstWebApp.Mappers;
using FirstWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TP.Models;

namespace FirstWebApp.Controllers
{
    public class AnimalController : Controller
    {
        public AnimalController()
        {
        }

        // GET: Animal
        public ActionResult Index()
        {
            // 1 - Récupération du modèle via le service métier (BLL)
            List<Animal> animaux = AnimalService.Instance.GetAnimaux();

            // 2 - Mapping du métier vers un view model dédié a la vue
            IEnumerable<AnimalViewModel> liste = animaux.Select(a => AnimauxMapper.MapModelToViewModel(a));

            #region Autres façons de faire la liste
            /* Autres façons de faire :
             * 
             * Façon 2 : 
             * IEnumerable<AnimalViewModel> liste = animaux.Select(a => AnimauxMapper.Map(a));
             * 
             * Façon 3 :
             * List<AnimalViewModel> nomsDesAnimaux = animaux.Select(a =>
             * {
             *      AnimalViewModel vm = AnimauxMapper.Map(a);
             *      return vm;
             * }).ToList();
             * 
             * Façon 4 :             
             * List<AnimalViewModel> liste2 = new List<AnimalViewModel>();
             * foreach (Animal animal in animaux)
             * {
             *     AnimalViewModel vm = AnimauxMapper.Map(animal);
             *     liste2.Add(vm);
             * }                         
             */
            #endregion

            // 3 - On retourne la vue "Index", avec la collection d'AnimalViewModel
            return View(liste);
        }

        // GET: Animal/Details/5
        public ActionResult Details(int? id)
        {
            Animal animal = AnimalService.Instance.GetAnimal(id);
            AnimalViewModel vm = AnimauxMapper.MapModelToViewModel(animal);
            return View(vm);
        }

        // GET: Animal/Create
        public ActionResult Create()
        {
            CreateUpdateAnimalViewModel vm = new CreateUpdateAnimalViewModel();

            // Chargement des listes de façon explicite
            vm.ChargerListes();

            return View(vm);
        }

        // POST: Animal/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUpdateAnimalViewModel vm)
        {
            /*
             * La classe CreateUpdateAnimalViewModel est instanciée par le 
             * framework .Net.
             * 
             * Les inputs du formulaire sont récupérés dynamiquement
             * pour renseigner les propriétés du VM.
             * 
             */
            try
            {
                // Si le modèle n'est pas valide
                if (!ModelState.IsValid)
                {
                    // Les listes sont vides car c'est le framework .Net
                    // qui a fait l'instance du VM
                    vm.ChargerListes();

                    return View(vm);
                }

                // Mapping du VM vers le Model
                Animal animal = AnimauxMapper.MapCreateUpdateViewModelToModel(vm);

                // Appel au service métier pour la création en base
                AnimalService.Instance.CreateAnimal(animal);

                // Redirection vers la page liste des animaux
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Utilisation d'une ressource
                ModelState.AddModelError("", Resources.Resource1.AnErrorOccured);

                vm.ChargerListes();

                return View(vm);
            }
        }

        // GET: Animal/Edit/5
        public ActionResult Edit(int? id)
        {
            // 1 - Récupération du modèle associé à l'id
            Animal animal = AnimalService.Instance.GetAnimal(id);

            if (animal == null)
            {
                return RedirectToAction("Index");
            }

            // 2 - Mapping du modèle vers le view modèle
            CreateUpdateAnimalViewModel vm = AnimauxMapper.MapModelToCreateUpdateViewModel(animal);

            vm.ChargerListes();

            // 3 - On retourne le view modèle à la vue
            return View(vm);
        }

        // POST: Animal/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreateUpdateAnimalViewModel vm)
        {
            try
            {
                // 1 - On récupère le modèle dans le contexte associé au view model
                Animal animal = AnimalService.Instance.GetAnimal(vm?.Id);

                if (animal == null)
                {
                    // On redirige sur l'action Index
                    // le nameof retourne Index sous la forme d'un string
                    return RedirectToAction(nameof(Index));
                }

                // Si jamais le modèle n'est pas valide
                if (!ModelState.IsValid)
                {
                    // Les listes sont nulles car le VM a été instancié 
                    // par le framework. On les recharge donc.
                    vm.ChargerListes();

                    // On retourne la vue avec le modèle associé
                    return View(vm);
                }

                // Si on est ici, les données reçues sont valides
                // On va mapper l'objet reçu vers la liste des objets de base
                AnimauxMapper.MapCreateUpdateViewModelToModel(vm, animal);

                // Appel au service métier pour persister les modifications
                AnimalService.Instance.UpdateAnimal(animal);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Utilisation d'une ressource
                ModelState.AddModelError("", Resources.Resource1.AnErrorOccured);

                vm.ChargerListes();

                return View(vm);
            }
        }

        // GET: Animal/Delete/5
        public ActionResult Delete(int? id)
        {
            Animal animal = AnimalService.Instance.GetAnimal(id);

            if (animal == null)
            {
                return RedirectToAction("Index");
            }

            AnimalViewModel vm = AnimauxMapper.MapModelToViewModel(animal);

            return View(vm);
        }

        // POST: Animal/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id, FormCollection formCollection)
        {
            try
            {
                AnimalService.Instance.DeleteAnimal(id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", Resources.Resource1.AnErrorOccured);

                Animal a = AnimalService.Instance.GetAnimal(id);
                
                if (a == null)
                {
                    return RedirectToAction("Index");
                }
                AnimalViewModel vm = AnimauxMapper.MapModelToViewModel(a);

                return View(vm);
            }
        }
    }
}
