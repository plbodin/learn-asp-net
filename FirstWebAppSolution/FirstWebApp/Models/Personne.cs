﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL.Models
{
    public class Personne
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public DateTime? DateCreation { get; set; }
        public DateTime? DateModification { get; set; }
        public int? UserCreation { get; set; }
        public int? UserModification { get; set; }

        public List<Animal> Animaux { get; set; }
    }
}