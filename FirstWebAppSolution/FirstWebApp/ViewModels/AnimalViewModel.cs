﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace FirstWebApp.ViewModels
{
    public class AnimalViewModel
    {
        public int Id { get; set; }

        [DisplayName("Nom de l'animal")]
        // ErrorMessageResourceName => Nom de clé de ressource utilisée
        // ErrorMessageResourceType => Nom du fichier de ressource utilisé
        // Cette ressource sera utilisée dans les formulaires utilisant cette propriété
        [Required(ErrorMessageResourceName = nameof(Resources.Resource1.AnimalNameRequired),
                  ErrorMessageResourceType = typeof(Resources.Resource1))]
        [MaxLength(30)]
        public string Nom { get; set; }

        [MaxLength(150)]
        public string Description { get; set; }

        /// <summary>
        /// Contient le nom du type d'animal associé a l'animal.
        /// </summary>
        public string TypeAnimal { get; set; }

        /// <summary>
        /// Contient la liste des couleurs, concaténées dans une chaîne
        /// de caractères.
        /// </summary>
        public string Couleurs { get; set; }
    }
}