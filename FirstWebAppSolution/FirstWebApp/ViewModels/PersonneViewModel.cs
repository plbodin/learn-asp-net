﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstWebApp.ViewModels
{
    public class PersonneViewModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public List<AnimalViewModel> Animaux { get; set; }
    }
}