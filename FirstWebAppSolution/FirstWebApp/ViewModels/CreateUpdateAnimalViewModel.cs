﻿using BLL.Models;
using BLL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace FirstWebApp.ViewModels
{
    public class CreateUpdateAnimalViewModel : AnimalViewModel
    {
        [Display(Name = "Couleurs")]
        public List<string> IdsCouleursSelectionnees { get; set; }
        public List<SelectListItem> ToutesLesCouleurs { get; set; }

        [Display(Name = "Type d'animal")]
        public string IdTypeAnimal { get; set; }
        public List<SelectListItem> TousLesTypesDanimaux { get; set; }

        public CreateUpdateAnimalViewModel()
        {
            Debug.WriteLine("Nouvelle instance de CreateUpdateAnimalViewModel.");
        }

        public void ChargerListes()
        {
            // On récupère les couleurs de la BLL
            List<Couleur> couleurs = AnimalService.Instance.GetCouleurs();

            // On les transforme en SelectListItem pour le composant UI
            this.ToutesLesCouleurs = couleurs.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Nom
            }).ToList();

            // On fait pareil pour les types d'animaux
            List<TypeAnimal> types = AnimalService.Instance.GetTypeAnimaux();
            this.TousLesTypesDanimaux = types.Select(t => new SelectListItem
            {
                Value = t.Id.ToString(),
                Text = t.Nom
            }).ToList();
        }
    }
}