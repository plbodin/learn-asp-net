﻿using BLL.Models;
using BLL.Services;
using FirstWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstWebApp.Mappers
{
    public class AnimauxMapper
    {
        public static AnimalViewModel MapModelToViewModel(Animal animal)
        {
            if (animal == null)
            {
                return null;
            }

            AnimalViewModel vm = new AnimalViewModel();

            MapModelToViewModel(animal, vm);

            return vm;

            // Mapping champ par champ
            /* return new AnimalViewModel
             {
                 Id = animal.Id,
                 Description = animal.Description,
                 Nom = animal.Nom,
                 Couleurs = string.Join(", ", animal.Couleurs?.Select(c => c.Nom)),
                 TypeAnimal = animal.TypeAnimal?.Nom
                 //IdTypeAnimal = animal.TypeAnimal?.Id.ToString(),
                 //// Pour chaque couleur, on récupère l'Id sous la forme d'un string
                 //IdsCouleursSelectionnees = animal.Couleurs?.Select(c => c.Id.ToString()).ToList()
             };*/
        }

        private static void MapModelToViewModel(Animal model, AnimalViewModel vm)
        {
            vm.Id = model.Id;
            vm.Nom = model.Nom;
            vm.Description = model.Description;
            // Le ?? permets de donner une valeur par défaut si la partie de gauche est nulle.            
            vm.Couleurs = string.Join(", ", model.Couleurs?.Select(c => c.Nom) ?? new List<string>());
            vm.TypeAnimal = model.TypeAnimal?.Nom;
        }

        public static CreateUpdateAnimalViewModel MapModelToCreateUpdateViewModel(Animal model)
        {
            CreateUpdateAnimalViewModel vm = new CreateUpdateAnimalViewModel();

            // Le CreateUpdateAnimalVM hérite de AnimalVM
            // On peut donc réutiliser le mapper de base
            MapModelToViewModel(model, vm);

            vm.IdTypeAnimal = model.TypeAnimal?.Id.ToString();
            vm.IdsCouleursSelectionnees = model.Couleurs?.Select(c => c.Id.ToString()).ToList();

            return vm;
        }

        public static Animal MapCreateUpdateViewModelToModel(CreateUpdateAnimalViewModel vm)
        {
            Animal animal = new Animal();

            MapCreateUpdateViewModelToModel(vm, animal);

            return animal;
        }

        public static void MapCreateUpdateViewModelToModel(CreateUpdateAnimalViewModel vm, Animal model)
        {
            // Mapping champ par champ
            model.Nom = vm.Nom;
            model.Description = vm.Description;
            // On a pas besoin de mapper l'Id

            // On récupère l'instance de TypeAnimal correspondant a l'Id sélectionné dans la vue
            TypeAnimal typeSelectionne = AnimalService.Instance.GetTypeAnimaux()
                                         .FirstOrDefault(t => vm.IdTypeAnimal == t.Id.ToString());

            // On associe par référence le type sélectionné au model
            model.TypeAnimal = typeSelectionne;

            // On récupère toutes les couleurs du service
            List<Couleur> toutesLesCouleurs = AnimalService.Instance.GetCouleurs();

            List<Couleur> couleursSelectionnees = null;

            // La liste peut être nulle dans le cas d'une sélection vide
            if (vm.IdsCouleursSelectionnees != null)
            {
                // On filtre ces couleurs en fonction des Ids des couleurs sélectionnées dans le VM
                couleursSelectionnees =
                    toutesLesCouleurs.Where(c => vm.IdsCouleursSelectionnees.Contains(c.Id.ToString()))
                    .ToList();
            }
            else
            {
                couleursSelectionnees = new List<Couleur>();
            }

            // On mets à jour le modèle avec la liste des couleurs sélectionnées
            model.Couleurs = couleursSelectionnees;
        }
    }
}