﻿using BLL;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemosEfConsole.Exemples
{
   public class OperationsAnimal
    {
        public static void AjouterAnimal(ContexteDeDonnee ctx)
        {
            // Chargement de tous les animaux de la base de données
            List<Animal> animaux = ctx.Animaux.ToList();

            int numeroCreation = animaux.Count + 1;
            Animal animal = new Animal
            {
                Nom = "Animal n°" + numeroCreation,
                Description = "Description de mon animal"
            };

            // Ajout du modèle créé au DbSet du contexte de données
            // Cela signifie que j'ajoute le modèle dans le contexte
            ctx.Animaux.Add(animal);

            // Le SaveChanges() ici va donc insérer en base l'animal
            // précédemment créé
            ctx.SaveChanges();
        }
    }
}
