﻿using BLL;
using BLL.Models;
using DemosEfConsole.Exemples;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DemosEfConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ContexteDeDonnee context = new ContexteDeDonnee();

            Animal premierChat = context.Animaux.First();

            TypeAnimal type = premierChat.TypeAnimal;
            List<Couleur> couleurs = premierChat.Couleurs;


            context.Animaux.Add(new Animal());
            context.Animaux.Add(new Animal());
            context.Animaux.Add(new Animal());
            context.SaveChanges();
           
            // Exemple de création
            OperationsAnimal.AjouterAnimal(context);


            // Je récupére la liste des animaux en BDD
            List<Animal> animauxEnBase = context.Animaux.ToList();


            Console.WriteLine("Animaux en base de données :");
            // Je boucle sur les animaux en base de données
            foreach(Animal animal in animauxEnBase)
            {
                // J'affiche l'animal dans la console
                Console.WriteLine(animal);
            }

            Console.ReadKey();
        }
    }
}
