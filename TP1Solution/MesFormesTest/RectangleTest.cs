﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MesFormes;


namespace MesFormesTest
{
    /// <summary>
    /// Attribut pour identifier les classes de test dans le code
    /// </summary>
    [TestClass]
    public class RectangleTest
    {
        /// <summary>
        /// Attribute pour définir la méthode comme étant un test unitaire
        /// </summary>
        [TestMethod]
        public void Perimetre_Test()
        {
            Rectangle r = new Rectangle();

            r.Largeur = 50;
            r.Longueur = 300;

            // Le périmètre du rectangle doit être de 700
            Assert.AreEqual(700, r.Perimetre);

            r.Largeur = 100;

            // Le périmètre doit maintenant être de 800
            Assert.AreEqual(800, r.Perimetre);         
        }

        [TestMethod]
        public void Aire_Test()
        {
            Rectangle r = new Rectangle();

            r.Largeur = 2;
            r.Longueur = 3;

            Assert.AreEqual(
                6,                
                r.Aire, 
                "L'aire du rectangle doit être de 6 car 2 fois 3 est égal à 6.");
        }
    }
}
