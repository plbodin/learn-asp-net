﻿namespace MesFormes
{
    public abstract class Forme
    {
        public abstract double Aire { get; }
        public abstract double Perimetre { get; }

        /// <summary>
        /// Surcharge de la méthode string ToString() 
        /// pour afficher l'aire suivie du périmètre
        /// </summary>
        /// <returns>L'aire et le périmètre sous la forme d'une chaine de caractères</returns>
        public override string ToString()
        {
            return 
                $"{this.GetSpecifitesForme()}Aire = {this.Aire}\nPérimètre = {this.Perimetre}\n";
        }

        protected virtual string GetSpecifitesForme()
        {
            return string.Empty;
        }
    }
}