﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesFormes
{
    public class Cercle : Forme
    {
        public int Rayon { get; set; }

        /// <summary>
        /// Formule Pi * Rayon²
        /// </summary>
        public override double Aire => Math.PI * Math.Pow(this.Rayon, 2);

        /// <summary>
        /// Formule 2 * Pi * Rayon
        /// </summary>
        public override double Perimetre => 2 * Math.PI * this.Rayon;

        protected override string GetSpecifitesForme()
        {
            return $"Cercle de rayon {this.Rayon}\n";
        }
    }
}
