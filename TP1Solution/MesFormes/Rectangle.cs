﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesFormes
{
    public class Rectangle : Forme
    {
        public virtual int Largeur { get; set; }
        public int Longueur { get; set; }
        
        /// <summary>
        /// Longueur * Largeur
        /// </summary>
        public override double Aire => this.Largeur * this.Longueur;

        /// <summary>
        /// Largeur + Longueur
        /// </summary>
        public override double Perimetre => 2 * (this.Largeur + this.Longueur);

        protected override string GetSpecifitesForme()
        {
            return $"Rectangle de longueur {this.Longueur} et de largeur {this.Largeur}\n";
        }
    }
}
