﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesFormes
{
    public class Carre : Rectangle
    {
        public override int Largeur => this.Longueur;       

        protected override string GetSpecifitesForme()
        {
            return $"Carré de côté {this.Longueur}\n";
        }
    }
}
