﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesFormes
{
    public class Triangle : Forme
    {
        /// <summary>
        /// Propriété privée avec getter seulement
        /// </summary>
        private double p => (this.Perimetre / 2);

        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }

        /// <summary>
        /// Application de la formule de Héron.
        /// </summary>
        public override double Aire =>
            Math.Sqrt(this.p * (this.p - this.A) * (this.p - this.B) * (this.p - this.C));

        /// <summary>
        /// Somme de tous les cotés
        /// </summary>
        public override double Perimetre => this.A + this.B + this.C;

        protected override string GetSpecifitesForme()
        {
            return $"Triangle de côté A = {this.A}, B = {this.B}, C = {this.C}\n";
        }
    }
}
