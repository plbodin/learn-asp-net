﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    /// <summary>
    /// Classe Personne
    /// </summary>
    public class Personne
    {
        /// <summary>
        /// Propriété publique de type DateTime, qui s'appelle DateNaissance
        /// </summary>
        public DateTime DateNaissance { get; set; }

        public string Prenom { get; set; }

       /* public string s;        
        public int? i;
        public Nullable<int> ii;
        public DateTime d;
        public DateTime? dd;
        public bool? b;
        public long? l;
        public float? f;
        public uint? ui;
        public byte? by;
        public byte[] bytes;*/

        private string _animalPrefere;
        public string AnimalPrefere
        {
            get { return this._animalPrefere; }
            set { this._animalPrefere = "Animal préféré = " + value; }
        }

        public Personne()
        {

        }

        /// <summary>
        /// Attribut (ou variable membre) qui contient le nom de la personne
        /// </summary>
        private string _nom;

        /// <summary>
        /// Propriété publique qui :
        /// - Retourne le membre privé _nom dans son Getter
        /// - Affecter le membre privé _nom dans son Setter
        /// </summary>
        public string Nom
        {
            // Accesseur de la propriété
            get
            {
                return this._nom;
            }
            // Modificateur/Mutateur de la propriété
            set
            {
                this._nom = value;
            }
        }

        public string Nom2
        {
            get
            {
                return this._nom;
            }            
        }

        public string Nom3
        {
            set
            {
                this._nom = value;
            }
        }
    }
}
