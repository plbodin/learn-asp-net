﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.Extensions
{
    public static class IntExtension
    {
        public static int Add(this int i, int j)
        {
            return i + j;
        }
    }
}
