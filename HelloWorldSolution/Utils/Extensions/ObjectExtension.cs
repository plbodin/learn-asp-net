﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.Extensions
{
    public static class ObjectExtension
    {
        /// <summary>
        /// Clone l'objet reçu en paramètre
        /// </summary>
        /// <typeparam name="T">Type générique</typeparam>
        /// <param name="itemToClone">Objet que l'on veut cloner</param>
        /// <returns>Item cloné</returns>
        public static T Clone<T>(this T itemToClone)
        {
            // On sérialize l'objet reçu en paramètre
            // => ça nous donne un string contenant une grappe de donnée Json
            string json = JsonConvert.SerializeObject(itemToClone);

            // On désérialize le json pour se voir affecter une nouvelle instance d'objet
            // à partir du Json
            T itemClone = JsonConvert.DeserializeObject<T>(json);

            return itemClone;
        }
    }
}
