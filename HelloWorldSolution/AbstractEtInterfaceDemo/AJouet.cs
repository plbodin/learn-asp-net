﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractEtInterfaceDemo
{
    public abstract class AJouet : object
    {
        protected int _heuresDeJeu;

        /// <summary>
        /// Protected permets de limiter l'accès a la méthode seulement
        /// aux classes de base
        /// </summary>
        protected abstract void Jouer();

        /// <summary>
        /// Une méthode "virtual" est une méthode qui peut être
        /// surchargée (overridée) dans une classe de base.
        /// On peut lui donner un comportement par défaut, et l'écraser
        /// en l'overridant.
        /// </summary>
        public virtual void Ranger()
        {
            Console.WriteLine("Rangement du jouet");
        }

        /// <summary>
        /// Méthode a appeler pour jouer avec précaution.        
        /// </summary>
        public void JouerAvecPrecaution()
        {
            try
            {
                this.Jouer();
            }
            catch (Exception ex)
            {
                // TODO logguer
            }
        }
    }
}
