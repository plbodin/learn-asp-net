﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractEtInterfaceDemo
{
    public class JeuDeDames : IJeuxSociete
    {
        public int NombreJoueurs => 2;
        // public int NombreJoueurs { get { return 2; } }

        public void LancerPartie()
        {
            Console.WriteLine("Jeu de dame lancé");
        }

        public override string ToString()
        {
            // nameof transforme une classe ou une propriété en string

            return string.Format("{1} - {0}", nameof(JeuDeDames), this.NombreJoueurs);
            return "JeuDeDames - " + this.NombreJoueurs;
            return string.Format("JeuDeDames - {0}", this.NombreJoueurs);
            return nameof(JeuDeDames);
            return "JeuDeDames";
            return $"{nameof(JeuDeDames)} - {this.NombreJoueurs}";
            return $"JeuDeDames - {this.NombreJoueurs}";
        }
    }
}
