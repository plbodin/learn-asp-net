﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractEtInterfaceDemo
{
    public class Peluche : AJouet
    {        
        protected override void Jouer()
        {
            Console.WriteLine("On joue avec la peluche");
        }

        public override string ToString()
        {
            return "Peluche";
        }
    }
}
