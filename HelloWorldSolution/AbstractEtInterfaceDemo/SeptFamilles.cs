﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractEtInterfaceDemo
{
    public class SeptFamilles : IJeuxSociete
    {
        public int NombreJoueurs => 7;

        public void LancerPartie()
        {
            Console.WriteLine("Sept familles lancé");
        }
    }
}
