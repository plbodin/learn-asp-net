﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractEtInterfaceDemo
{
    public interface IJeuxSociete
    {
        /// <summary>
        /// Propriété avec un getter seulement.
        /// </summary>
        int NombreJoueurs { get; }

        void LancerPartie();
    }
}
