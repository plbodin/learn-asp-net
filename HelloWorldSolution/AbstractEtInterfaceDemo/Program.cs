﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractEtInterfaceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instanciation d'une liste de jeux sur plusieurs lignes
            List<IJeuxSociete> jeuxSocietes = new List<IJeuxSociete>();
            JeuDeDames dames = new JeuDeDames();
            SeptFamilles septFamilles = new SeptFamilles();

            jeuxSocietes.Add(dames);
            jeuxSocietes.Add(septFamilles);           

            // Inline instanciation d'une collection de IJeuxSociete
            jeuxSocietes = new List<IJeuxSociete>
            {
                new JeuDeDames(),
                new SeptFamilles()
            };

            jeuxSocietes.RemoveAt(1);

            // Boucle sur les jeux de société
            foreach (IJeuxSociete jeu in jeuxSocietes)
            {
                // Appel à la méthode de l'interface
                // Cet appel est fait sur les méthodes implémentées 
                // dans les classes JeuDeDames et SeptFamilles
                // jeu.LancerPartie();

                Console.WriteLine(jeu);
            }

            List<AJouet> jouets = new List<AJouet>
            {
                new Peluche(),
                new PlayMobile()
            };

            foreach(AJouet jouet in jouets)
            {
                jouet.JouerAvecPrecaution();

                Console.WriteLine(jouet);
                Debug.WriteLine(jouet);
            }

            Console.ReadLine();
        }
    }
}
