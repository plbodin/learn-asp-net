﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Données en entrée
            int[] numbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int un = numbers.FirstOrDefault();
            int neuf = numbers.LastOrDefault();

            // On récupère le premier élément supérieur strict a 3
            int quatre = numbers.FirstOrDefault(n => n > 3);
            // même ligne mais différente
            quatre = numbers.Where(n => n > 3).FirstOrDefault();

            // On itère la collection "numbers"
            // Pour chaque number "n", on sélectionne une nouvelle instance
            // de la classe Nombre, tout en lui affectant a la propriété "Valeur"
            // le nombre "n"
            Nombre[] nombres = numbers.Select(n => new Nombre { Valeur = n }).ToArray();

            // équivalent de la ligne du dessus, mais avec un Select sur plusieurs lignes
            Nombre[] nombres2 = numbers.Select(n =>
            {
                return new Nombre { Valeur = n };
            }).ToArray();

            // On récupère le max de la propriété Valeur dans la liste de nombre
            int valeurMax = nombres.Max(n => n.Valeur);

            // S'écrit également avec un Select avant l'appel a Max            
            valeurMax = nombres.Select(n => n.Valeur).Max();

            // Récupération des Min/Max/Average sur la collection numbers 
            int min = numbers.Min();
            int max = numbers.Max();
            double average = numbers.Average();

            List<int> emptyList = new List<int>();

            // Récupération des Min/Max/Average sur une collection vide
            // Le DefaultIfEmpty() permets de robuster le code avant l'appel aux extensions Min/Max/Average
            min = emptyList.DefaultIfEmpty().Min();
            max = emptyList.DefaultIfEmpty().Max();
            average = emptyList.DefaultIfEmpty().Average();

            // Création d'un tableau avec doublons
            numbers = new int[] { 1, 1, 2, 2, 3, 3, 4, 4, 5 };

            int[] deuxPremiers = numbers.Take(2).ToArray();
            Console.WriteLine("2 premiers entiers : " + string.Join(", ", deuxPremiers));

            int[] deuxPremiersUniques = numbers.Distinct().Take(2).ToArray();
            int[] deuxPremiersAPartirDeLindex1 = numbers.Skip(1).Take(2).ToArray();

            // Tri de la collection nombres dans l'ordre décroissant de la valeur
            Nombre[] nombresTries = nombres.OrderByDescending(n => n.Valeur).ToArray();
            Console.WriteLine("Nombres triés : " + string.Join(",", nombresTries.Select(n => n.Valeur)));

            // Tri de la collection nombres dans l'ordre croissant de la propriété Random
            // puis dans l'ordre décroissant de la propriété Valeur
            Nombre[] nombresTriesAutrement = nombres.OrderBy(n => n.Random)
                                                    .ThenByDescending(n => n.Valeur)
                                                    .ToArray();

            // Pour chaque nombre, on sélectionne une chaîne de caractères construites sur le tas
            // via le $"...."
            IEnumerable<string> affichage
                = nombresTriesAutrement.Select(n => $"R={n.Random}, V={n.Valeur}");

            // On "Join" chaque item de la liste, avec comme séparateur un saut de ligne
            string s = string.Join("\n", affichage);

            // Affichage dans la console
            Console.WriteLine(s);

            // La méthode All retourne un booléen indiquant si tous les items de la collection
            // respectent le prédicat.
            bool tousNombresInferieursA100 = numbers.All(n => n < 100);

            // La méthode Any retourne un booléen indiquand si au moins 1 élement de la liste
            // respecte le prédicat.
            bool anyNumberPositive = numbers.Any(n => n > 0);

            // Liste arbitraire de parents
            List<Parent> parents = new List<Parent>
            {
                new Parent { Nom = "Parent 1" },
                new Parent
                {
                    Nom = "Parent 2",
                    Filles = new List<Fille>
                    {
                        new Fille { Nom = "Fille 1 du parent 2" },
                        new Fille { Nom = "Fille 2 du parent 2" }
                    }
                },
                new Parent
                {
                    Nom = "Parent 3",
                    Filles = new List<Fille>
                    {
                        new Fille { Nom = "Fille 1 du parent 3" }
                    }
                }
            };

            List<Fille> filles = new List<Fille>();

            // Old school way
            foreach (Parent parent in parents)
            {
                if (parent.Filles != null)
                {
                    foreach (Fille fille in parent.Filles)
                    {
                        filles.Add(fille);
                    }
                }
            }

            // Faster way
            // On part de la liste des parents
            // On filtre les parents qui ont des filles non nulles
            // On sélectionne (avec le SelectMany) les collections filles des parents
            List<Fille> filles2 = parents.Where(p => p.Filles != null)
                                         .SelectMany(p => p.Filles)                                         
                                         .ToList();

            List<List<Fille>> fillesencore = parents.Where(p => p.Filles != null)
                                         .Select(p => p.Filles)                                        
                                         .ToList();

            Console.ReadKey();
        }
    }
}
