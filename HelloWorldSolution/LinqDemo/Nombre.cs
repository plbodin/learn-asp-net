﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo
{
    public class Nombre
    {
        private static Random random = new Random();

        public int Valeur { get; set; }

        public int Random { get; private set; }

        public Nombre()
        {
            // Appel a la propriété statique contenant le random
            // => une propriété statique est partagée pour toutes les instances de la classe
            // Le .Next(1,10) retourn un nombre aléatoire entre 1 et 10
            this.Random = Nombre.random.Next(1, 10);
        }
    }
}
