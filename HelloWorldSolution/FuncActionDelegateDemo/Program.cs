﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncActionDelegateDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            // Le func permet de mettre dans une variable une méthode
            // On peut "ballader" cette variable sans l'appeler
            // c'est même fait pour ça
            Func<int> getRandom = new Func<int>(() =>
            {
                return r.Next();
            });

            // UTILISATION D'UNE EXPRESSION LAMBDA :
            //
            // Les "()" correspondent aux paramètres d'entrée
            // Le "=>" se mets entre les paramètres d'entrée et le corps de la méthode
            // La méthode peut s'écrire de deux manières :
            //  - Soit sur une seule ligne, dans ce cas, il y return implicite
            //  - Soit sur plusieurs lignes, dans ce cas la méthode doit être écrite
            //    entre { }, avoir des ";" a la fin des lignes, et avoir une condition
            //    de sortie (un return).
            Func<int> getRandomInt = new Func<int>(() => r.Next());

            int r1 = getRandomInt();
            int r2 = getRandomInt();
            Console.WriteLine("Random 1 = {0}, Random 2 = {1}", r1, r2);

            Func<int, int, int> getRandomInt2 = new Func<int, int, int>(
                (i1, i2) => r.Next(i1, i2));

            int r3 = getRandomInt2(0, 10);
            int r4 = getRandomInt2(10, 100);
            Console.WriteLine("Random 3 = {0}, Random 4 = {1}", r3, r4);

            // Création d'une action sans paramètre d'entrée, qui affiche dans la console
            // un message lorsqu'on exécute l'action.
            Action action1 = new Action(() => Console.WriteLine("Exécution de l'action 1."));
            
            // Exécution de l'action comme une méthode,
            // Avec les "()" a la suite de la variable.
            action1();

            // Création d'une action qui prends un seul paramère en entrée, de type string
            Action<string> sayHello = new Action<string>(s =>
            {
                Console.WriteLine($"Bienvenue {s}");
            });

            // Exécution de l'action, avec un passage de paramètre obligatoire
            sayHello("Toto");

            Console.WriteLine("Veuillez entrer votre nom :");
            string nom = Console.ReadLine();
            sayHello(nom);

            Console.ReadKey();
        }
    }
}
