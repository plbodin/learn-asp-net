﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;
using Utils.Extensions;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ctrl+K et Ctrl+D => réindentation automatique du code
            Class1.WriteHelloWorld();

            // Instanciation de la classe Personn
            Personne p = new Personne
            {
                Nom = "Toto",
                // On passe par le Setter pour "setter" la valeur "Le chat"
                AnimalPrefere = "Le chat",
                DateNaissance = new DateTime(2000, 5, 13),
                Prenom = "Thierry"
            };

            Personne p2 = new Personne { Nom = "Titi", Prenom = "Henry" };

            // On appele le Getter pour récupérer la valeur de la propriété           
            Console.WriteLine(p.AnimalPrefere);

            // On associe p a la même référence que p2
            p = p2;

            // Si on modifie p, p2 est également modifié => c'est la même référence d'objet
            p.Nom = "Toto";

            Console.WriteLine(p2.Nom);

            Console.WriteLine(p.AnimalPrefere);

            // Le Clone permet de casser la référence pour travailler sur 2 instances distinctes
            // Appel a la méthode clone static (juste en bas)
            object o = Clone(p2);
            // Cast de "o" en tant que classe Personne
            Personne pCloneDeP2 = o as Personne;

            // Appel a la méthode d'extenstion Clone()
            Personne pCloneDeP2bis = p2.Clone(); 

            int i = 5;

            // Appel a la méthode d'extension Add, contenue dans 
            // le namespace Utils.Extensions
            int neuf = i.Add(4);            

            Console.ReadLine();
        }

        /// <summary>
        /// Clone l'objet reçu en paramètre
        /// </summary>
        /// <typeparam name="T">Type générique</typeparam>
        /// <param name="itemToClone">Objet que l'on veut cloner</param>
        /// <returns>Item cloné</returns>
        private static object Clone(object itemToClone)
        {
            // On sérialize l'objet reçu en paramètre
            // => ça nous donne un string contenant une grappe de donnée Json
            string json = JsonConvert.SerializeObject(itemToClone);

            // On désérialize le json pour se voir affecter une nouvelle instance d'objet
            // à partir du Json
            object itemClone = JsonConvert.DeserializeObject(json);

            return itemClone;
        }
    }
}
