﻿using ProjetLinq.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            // On force l'encoding de la console a UTF8
            Console.OutputEncoding = Encoding.UTF8;

            InitialiserDatas();

            // o	Afficher la liste des prénoms des auteurs dont le nom commence par "G"
            IEnumerable<string> prenoms = ListeAuteurs
                                         .Where(a => a.Nom != null && a.Nom.ToUpper().StartsWith("G"))
                                         .Select(a => a.Prenom);

            Console.WriteLine("Requête 1 - liste des prénoms des auteurs dont le nom commence par G:\n" + string.Join(", ", prenoms));

            // o	Afficher l’auteur ayant écrit le plus de livres 

            // On groupe les livres par auteur
            // L'auteur devient la clé d'une liste d'auteurs avec leurs livres
            // On est sur une liste de clé => valeurs
            Auteur auteurRequete2 = ListeLivres.GroupBy(l => l.Auteur)
                               // La clé/valeurs est nommée g et est de type IGrouping<Auteur, Livre>
                               // On trie la liste des clés/valeurs de manière décroissante 
                               // sur le nombre de livres
                               .OrderByDescending(g => g.Count())
                               // On sélectionne la Clé, qui est l'auteur
                               .Select(g => g.Key)
                               // On récupère seulement le premier auteur de cette liste
                               .FirstOrDefault();

            Console.WriteLine("\nRequête 2 - auteur ayant écrit le plus de livres:\n" + auteurRequete2);

            // Correction ENI
            {                
                var auteurPlusDeLivres = ListeLivres.GroupBy(l => l.Auteur)
                                                    .OrderByDescending(g => g.Count())
                                                    // On récupère le premier élément ou null (default)                                                    
                                                    .FirstOrDefault()
                                                    // Dans le cas du default, on récupérera une valeur
                                                    // nulle. En rajoutant un "?" avant d'appeler la propriété
                                                    // On s'assure de pas recevoir un NullReferenceException
                                                    ?.Key;
                Console.WriteLine("Quel auteur a écrit le plus de livres");
                Console.WriteLine($"{auteurPlusDeLivres.Prenom} {auteurPlusDeLivres.Nom}");
                Console.WriteLine();
            }
            // o	Afficher le nombre moyen de pages par livre par auteur
            Console.WriteLine("\nRequête 3 - le nombre moyen de pages par livre par auteur:");
            foreach (Auteur auteur in ListeAuteurs)
            {
                List<Livre> livresAuteur = ListeLivres.Where(l => l.Auteur == auteur).ToList();
                double moyenne = livresAuteur.Select(l => l.NbPages).DefaultIfEmpty().Average();
                Console.WriteLine($"Auteur : {auteur}, moyenne des pages : {moyenne}");
            }

            {
                // Utilisation d'un type anonyme déclaré sur le tas
                var anonymousType = new { Parametre1 = "Param1", Valeur = 2 };
            }

            var resultaltRequete3 = ListeAuteurs.Select(a => new
            {
                Auteur = a,
                NbPagesMoyennes = ListeLivres.Where(l => l.Auteur == a)
                                              .Select(l => l.NbPages)
                                              .DefaultIfEmpty()
                                              .Average()
            });

            Console.WriteLine(string.Join(", ", resultaltRequete3.Select(r => $"{r.Auteur} => {r.NbPagesMoyennes}")));


            // o	Afficher le titre du livre avec le plus de pages
            string titreLivre = ListeLivres.OrderByDescending(l => l.NbPages)
                                           .Select(l => l.Titre)
                                           .FirstOrDefault();
            Console.WriteLine("\nRequête 4 : Livre ayant le plus de pages : " + titreLivre);

            // o	Afficher combien ont gagné les auteurs en moyenne (moyenne des factures)
            decimal moyenneDesFactures = ListeAuteurs.SelectMany(a => a.Factures)
                                                    .Select(f => f.Montant)
                                                    .DefaultIfEmpty()
                                                    .Average();
            Console.WriteLine(string.Format("\nRequête 5 : Moyenne des factures = {0:C2}", moyenneDesFactures));

            // o	Afficher les auteurs et la liste de leurs livres
            IEnumerable<string> auteursEtLeursLivres = ListeAuteurs.Select(a => new
            {
                Auteur = a,
                Livres = ListeLivres.Where(l => l.Auteur == a)
            })
                .Select(a => $"{a.Auteur}, a écrit les livres suivants : {string.Join(", ", a.Livres) }");

            Console.WriteLine("\nRequête 6 : auteurs et la liste de leurs livres :");
            Console.WriteLine(string.Join("\n", auteursEtLeursLivres));

            // o	Afficher les titres de tous les livres triés par ordre alphabétique
            IEnumerable<string> livresTries = ListeLivres.Select(l => l.Titre)
                                                         .OrderBy(titre => titre);
            // Autre façon de faire, d'abord trier puis sélectionner
            livresTries = ListeLivres.OrderBy(l => l.Titre).Select(l => l.Titre);

            Console.WriteLine("\nRequête 7 : titres de tous les livres triés par ordre alphabétique :");
            Console.WriteLine(string.Join("\n", livresTries));

            // o	Afficher la liste des livres dont le nombre de pages est supérieur à la moyenne
            double moyenneDesPages = ListeLivres.Select(l => l.NbPages)
                                                .DefaultIfEmpty()
                                                .Average();

            // On récupère les livres dont le NbPages > moyenneDesPages
            IEnumerable<Livre> livresSuperieursAMoyenne =
                 ListeLivres.Where(l => l.NbPages > moyenneDesPages);
            Console.WriteLine("\nRequête 8 : livres dont le nombre de pages est supérieur à la moyenne :");
            Console.WriteLine(string.Join("\n", livresSuperieursAMoyenne));

            // o	Afficher l'auteur ayant écrit le moins de livres
            Auteur auteurAyantEcrisLeMoinsDeLivre =
                      // En premier on pars de la liste des auteurs
                      ListeAuteurs
                      // Pour chaque auteur, on crée un type anonyme
                      // comprenant d'une part l'auteur, et d'une autre
                      // part la liste des livres qu'il a écrit
                      .Select(auteur => new
                      {
                          Auteur = auteur,
                          Livres = ListeLivres.Where(l => l.Auteur == auteur)
                      })
                      // Ensuite on trie cette collection de Auteur/Livres
                      // dans l'ordre croissant du nombre de livres
                      .OrderBy(anonym => anonym.Livres.Count())
                      // Sélection de l'Auteur
                      .Select(anonym => anonym.Auteur)
                      // On prends le premier auteur ayant écris le moins de livre
                      .FirstOrDefault();

            // Autre manière de faire, avec un foreach et un stockage d'informations
            // dans un dictionnaire
            Dictionary<Auteur, List<Livre>> dico = new Dictionary<Auteur, List<Livre>>();
            foreach (Auteur auteur in ListeAuteurs)
            {
                List<Livre> livres = ListeLivres.Where(l => l.Auteur == auteur).ToList();
                dico.Add(auteur, livres);
            }

            auteurAyantEcrisLeMoinsDeLivre = dico.OrderBy(d => d.Value.Count)
                                                 .Select(d => d.Key)
                                                 .FirstOrDefault();

            Console.WriteLine("\nRequête 9 : auteur ayant écrit le moins de livres : ");
            Console.WriteLine(auteurAyantEcrisLeMoinsDeLivre);

            // Pour attendre une action utilisateur avant de fermer le programme
            Console.ReadKey();
        }

        private static List<Auteur> ListeAuteurs = new List<Auteur>();
        private static List<Livre> ListeLivres = new List<Livre>();

        private static void InitialiserDatas()
        {
            ListeAuteurs.Add(new Auteur("GROUSSARD", "Thierry"));
            ListeAuteurs.Add(new Auteur("GABILLAUD", "Jérôme"));
            ListeAuteurs.Add(new Auteur("HUGON", "Jérôme"));
            ListeAuteurs.Add(new Auteur("ALESSANDRI", "Olivier"));
            ListeAuteurs.Add(new Auteur("de QUAJOUX", "Benoit"));
            ListeLivres.Add(new Livre(1, "C# 4", "Les fondamentaux du langage", ListeAuteurs.ElementAt(0), 533));
            ListeLivres.Add(new Livre(2, "VB.NET", "Les fondamentaux du langage", ListeAuteurs.ElementAt(0), 539));
            ListeLivres.Add(new Livre(3, "SQL Server 2008", "SQL, Transact SQL", ListeAuteurs.ElementAt(1), 311));
            ListeLivres.Add(new Livre(4, "ASP.NET 4.0 et C#", "Sous visual studio 2010", ListeAuteurs.ElementAt(3), 544));
            ListeLivres.Add(new Livre(5, "C# 4", "Développez des applications windows avec visual studio 2010", ListeAuteurs.ElementAt(2), 452));
            ListeLivres.Add(new Livre(6, "Java 7", "les fondamentaux du langage", ListeAuteurs.ElementAt(0), 416));
            ListeLivres.Add(new Livre(7, "SQL et Algèbre relationnelle", "Notions de base", ListeAuteurs.ElementAt(1), 216));
            ListeAuteurs.ElementAt(0).addFacture(new Facture(3500, ListeAuteurs.ElementAt(0)));
            ListeAuteurs.ElementAt(0).addFacture(new Facture(3200, ListeAuteurs.ElementAt(0)));
            ListeAuteurs.ElementAt(1).addFacture(new Facture(4000, ListeAuteurs.ElementAt(1)));
            ListeAuteurs.ElementAt(2).addFacture(new Facture(4200, ListeAuteurs.ElementAt(2)));
            ListeAuteurs.ElementAt(3).addFacture(new Facture(3700, ListeAuteurs.ElementAt(3)));
        }
    }
}
